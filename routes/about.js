var express = require('express');
var router = express.Router();
const controller = require ('../controllers/controllers');


router.get('/', controller.renderAboutPage);

module.exports = router;
