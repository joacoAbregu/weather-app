const axios = require("axios");
const Weather = require("../models/weather");


exports.renderIndexPage = (req, res) => {
    res.render("index")
}

exports.getWeather = (req, res) => {  
    const city = req.body.city;
    const url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${process.env.APIKEY}&units=metric`
    const weather = new Weather(req.body.city);
    weather.validateUserInput();

    if(weather.errors.length){
        res.render("index", {
            error: weather.errors.toString()
        })
    } else{
        axios.get(url).then((response) => {
            res.render("index", {
                weather: `It's currently ${response.data.main.temp} in ${response.data.name}`
            })
        }).catch((error) => {
            console.log(error)
        });
    }
    
}

exports.renderAboutPage = (req, res) => {
    res.render("about")
}