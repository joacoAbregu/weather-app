var express = require('express');
var router = express.Router();
const controller = require ('../controllers/controllers');

/* GET home page. */
router.get('/', controller.renderIndexPage);
router.post('/', controller.getWeather);

module.exports = router;
